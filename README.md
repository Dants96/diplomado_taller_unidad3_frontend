# Hogar de Paso Mascotas front-end

# Video de la aplicacion en funcionamiento
https://drive.google.com/file/d/1yMKTUXkTNr2te_mRCv5gOcyv4wES4-ro/view?usp=sharing

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



