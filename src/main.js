/*import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
//import Header from './components/Header.vue'

createApp(App).use(router).mount('#app')
//createApp(Header).mount('#header')*/



import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css';
//import Header from './components/Header.vue'

createApp(App).use(router).use(VueSweetalert2).mount('#app')
